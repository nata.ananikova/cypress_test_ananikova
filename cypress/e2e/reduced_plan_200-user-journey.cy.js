//Уменьшаемый план user-journey
context('logging in', () => {

  it('Can navigate to home page', () => {
    
    cy.visit('https://finance.dev.fabrique.studio/accounts/login/', {
  auth: {
    username: 'fabrique',
    password: 'fabrique',
  },
    })
    cy.get('input[type="email"]').type('admin@admin.ad')
    cy.get('input[type="password"]').type('admin')
    cy.get('[class="button__content"]').click()

    
    cy.contains("Добавить платёж").click()

    cy.get('[class="checkbox__label"]').contains("Расход").click() 
 
    cy.get('textarea[class="input__input"]').first().type('Долговые обязательства')
    cy.get('div[class="input input--is-short input--size-md input--state-default"]').first().type('200')
    cy.get('div[class="input input--is-short input--size-md input--state-default"]').last().type('200')

    cy.get('div[class="checkbox__label"]').contains("Оплачен").click() 

    cy.get('div[class="date date--is-short date--state-undefined"]').first().type('12.07.2023')
    cy.get('div[class="date date--is-short date--state-undefined"]').last().type('12.07.2023')

    cy.get('textarea[class="input__input"]').last().type('Монтажник Иванов')

    cy.get('button[class="button button--size-sm button--is-centered button--state-filled"]').contains("Добавить").click()

  })
})

